cmake_minimum_required(VERSION 3.6.0 FATAL_ERROR)

project (pigcd CXX)

set (pigcd_VERSION_MAJOR 0)
set (pigcd_VERSION_MINOR 1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
  #set(CMAKE_BUILD_TYPE Debug)
endif()

#add_compile_definitions(OPENCV_VERSION=${OpenCV_VERSION}) 
add_compile_definitions(ARCH_${CMAKE_HOST_SYSTEM_PROCESSOR})

include(CheckLibraryExists)



set(CMAKE_CXX_FLAGS "-Wall -Wextra -fopenmp")
set(CMAKE_CXX_FLAGS_DEBUG "-ggdb ")
# consider also -flto
set(CMAKE_CXX_FLAGS_RELEASE "-O3 -march=native -frename-registers -funroll-loops -DNDEBUG")


# install(DIRECTORY include/ DESTINATION include/pigcd
#         FILES_MATCHING PATTERN "*.hpp")

find_package(Git)
find_package(Threads)
find_package(Doxygen 
             OPTIONAL_COMPONENTS mscgen dia dot)

file(DOWNLOAD https://raw.githubusercontent.com/pantadeusz/distance_t/0b212d33c76ba612c924147f7b34c33f51508712/distance_t.hpp           ${CMAKE_CURRENT_BINARY_DIR}/thirdparty/distance_t/distance_t.hpp)
file(DOWNLOAD https://raw.githubusercontent.com/nlohmann/json/6d8d043adde8c95589f6e65e1fe228cb1f0d8419/single_include/nlohmann/json.hpp ${CMAKE_CURRENT_BINARY_DIR}/thirdparty/json/json.hpp )

include_directories("${PROJECT_SOURCE_DIR}/src")
include_directories("${CMAKE_CURRENT_BINARY_DIR}/thirdparty/distance_t")
include_directories("${CMAKE_CURRENT_BINARY_DIR}/thirdparty/json")


set(pigcd_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/src/pigcd.cpp )
file(GLOB_RECURSE lib_SOURCES "src/*.cpp" "src/*/*.cpp")
list(REMOVE_ITEM lib_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/src/raspigcd.cpp)

add_executable(pigcd ${pigcd_SOURCES} ${lib_SOURCES})

install (TARGETS pigcd DESTINATION bin)

SET(CPACK_PACKAGE_VERSION_MAJOR ${pigcd_VERSION_MAJOR})
SET(CPACK_PACKAGE_VERSION_MINOR ${pigcd_VERSION_MINOR})
SET(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_SOURCE_DIR}/README.md")
SET(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
SET(CPACK_GENERATOR "DEB" "TGZ")

SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "Tadeusz Puźniakowski")
INCLUDE(CPack)
 
# To use this:
# make package

