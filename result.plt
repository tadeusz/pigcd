# 

set terminal pngcairo size 8000,512 enhanced font 'Verdana,10'

set output 'result_ticks.png'
plot 'result.txt' u 33:27 w lines, 'result.txt' u 33:22 w lines

set terminal pngcairo size 1024,968 enhanced font 'Verdana,10'

set output 'result_track_3d.png'
splot 'result.txt' u 34:35:36 w lines

set output 'result_track_2d.png'
plot 'result.txt' u 34:35 w lines

set output 'result_track_2d_time.png'
splot 'result.txt' u 34:35:33 w lines

set terminal pngcairo size 8000,512 enhanced font 'Verdana,10'
set output 'result_track_velocity_x_y_z.png'
plot 'result.txt' u 33:36,'result.txt' u 33:37,'result.txt' u 33:38 w lines
