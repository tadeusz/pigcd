/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "configuration.hpp"

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>

#include "buttons.hpp"
#include "motors.hpp"
#include "unit_conversion.hpp"
#include "webapi.hpp"

using namespace std;

namespace pigcd {
namespace config {

std::string config_file_name = "config.cfg";

std::map<std::string, std::function<void(std::string)>> options_setters;
std::map<std::string, std::function<std::string()>> options_getters;
std::map<std::string, std::string> options_descriptions;

bool register_setting(std::string name, const std::function<void(std::string)> &setter, const std::function<std::string()> &getter,
                      const std::string description) {
    if (options_setters.count(name) != 0) throw std::invalid_argument("setting " + name + " already registered");
    options_setters[name] = setter;
    options_getters[name] = getter;
    options_descriptions[name] = description;
    return true;
}

#define REGISTER_APP_SETTING_ARRAY(paramname, description)                                                                               \
    config::register_setting(                                                                                                            \
        #paramname,                                                                                                                      \
        [](std::string value) -> void {                                                                                                  \
            std::stringstream s(value);                                                                                                  \
            unsigned i = 0;                                                                                                              \
            auto tmp = paramname.at(i);                                                                                                  \
            decltype(tmp) a;                                                                                                             \
            while (s >> a) paramname.at(i++) = a;                                                                                        \
            if (i != paramname.size())                                                                                                   \
                throw std::invalid_argument(std::string(#paramname) + " should have " + std::to_string(paramname.size()) + " elements"); \
        },                                                                                                                               \
        []() -> std::string {                                                                                                            \
            std::stringstream ret;                                                                                                       \
            for (auto e : paramname) ret << " " << e;                                                                                    \
            return ret.str().substr(1);                                                                                                  \
        },                                                                                                                               \
        description);

#define REGISTER_APP_SETTING_VALUE(paramname, description) \
    config::register_setting(                              \
        #paramname,                                        \
        [](std::string value) -> void {                    \
            std::stringstream s(value);                    \
            s >> paramname;                                \
        },                                                 \
        []() -> std::string {                              \
            std::stringstream s;                           \
            s << paramname;                                \
            return s.str();                                \
        },                                                 \
        description);

std::string machine_name = "default";

bool arguments_ready = false;

void setup() {
    using namespace pigcd;
    using namespace std::chrono_literals;

    // xyz - pins - only at start!
    REGISTER_APP_SETTING_ARRAY(motors::io_steppers_dir, "Stepper mottors dir pin, for example '22 17 11'");
    REGISTER_APP_SETTING_ARRAY(motors::io_steppers_en, "Stepper motors enable pin");
    REGISTER_APP_SETTING_ARRAY(motors::io_steppers_step, "Stepper motors step pin");
    REGISTER_APP_SETTING_ARRAY(motors::io_spindles, "Spindle on/off pin");

    REGISTER_APP_SETTING_ARRAY(buttons::io_buttons, "Button pins - four are supported");

    // this can be changed in runtime
    REGISTER_APP_SETTING_ARRAY(unit_conversion::steps_per_millimeter, "steps per millimeter for each axis motor");
    REGISTER_APP_SETTING_VALUE(motors::step_duration, "duration between steps in nanoseconds. This is the minimal delay between ticks");

    REGISTER_APP_SETTING_VALUE(machine_name, "The name of the machine reported by API");
    REGISTER_APP_SETTING_VALUE(config_file_name, "The configuration filename. The default is ./config.cfg");

    REGISTER_APP_SETTING_VALUE(webapi::static_files_directory, "The directory name containing static files for web interface demo. Default is 'static'");
    REGISTER_APP_SETTING_VALUE(webapi::server_host, "API address. 0.0.0.0 means any address.");
    REGISTER_APP_SETTING_VALUE(webapi::server_port, "API port. Default is 12112.");

    arguments_ready = true;
}

int from_args(int argc, char **argv) {
    if (!arguments_ready) setup();
    int arguments_set = 0;
    if (argc > 1) {
        std::vector<std::string> arguments(argv + 1, argv + argc);
        for (auto arg = arguments.begin(); arg != arguments.end(); arg++) {
            auto conf_key = *arg;
            if ((conf_key == "-h") || (conf_key == "--help") || (conf_key == "-help")) {
                std::cout << generate_help_screen(
                    "pigcd argumentname 'argumentvalue'",
                    std::string("example:\n    pigcd webapi::static_files_directory '../static'\n\n              build ") + __DATE__, 80);
                arguments_set = -1;
            } else if (options_setters.count(conf_key)) {
                auto setter = options_setters[conf_key];
                arg++;
                if (arg != arguments.end()) {
                    setter(*arg);
                    arguments_set++;
                } else
                    throw std::invalid_argument("you did not provide arguments for " + conf_key + ". Example value '" + options_getters[conf_key]() + "'");
            }
        }
    }
    return arguments_set;
}
int load(std::istream &cfg) {
    if (!arguments_ready) setup();
    using namespace pigcd;
    using namespace std::chrono_literals;

    string line;
    while (getline(cfg, line)) {
        stringstream ss(line);
        string name;
        ss >> name;
        if (options_setters.count(name)) {
            options_setters[name](line.substr(name.size() + 1));
        } else if ((!((name.size() > 0) && (name[0] == '#'))) && !(name.size() == 0)) {
            std::cerr << "Unrecognized config line: " << line << std::endl;
        }
    }
    return 0;
}

int save(std::ostream &f) {
    if (!arguments_ready) setup();
    using namespace pigcd;
    using namespace std::chrono_literals;
    for (auto [name, getter] : options_getters) {
        f << name << " " << getter() << std::endl;
    }
    return 0;
}

std::string generate_help_screen(std::string header, std::string footer, unsigned screen_width) {
    std::stringstream ss;
    ss << header << std::endl << std::endl;
    unsigned max_param_name_length = 20;
    for (auto [paramname, description] : options_descriptions) {
        if (max_param_name_length < paramname.size()) max_param_name_length = paramname.size();
    }

    for (auto [paramname, description] : options_descriptions) {
        ss << "    " << paramname << std::endl;
        while (description.length() > 0) {
            int l = std::min((int)(screen_width - 8), (int)(description.size()));
            auto description_part = description.substr(0, l);
            if ((l != 0) && (l == ((int)screen_width - 8)) && (description_part.find_last_of(' ') != std::string::npos)) {
                l = std::max((int)1, (int)description_part.find_last_of(' ') + 1);
            }

            ss << "        " << description.substr(0, l) << std::endl;
            description = description.substr(l);
        }
        ss << std::endl;
    }

    ss << footer << std::endl;
    return ss.str();
}

}  // namespace config
}  // namespace pigcd