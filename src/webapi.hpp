#ifndef ___WEBAPI__HPP_____
#define ___WEBAPI__HPP_____
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <any>
#include <atomic>
#include <functional>
#include <list>
#include <memory>
#include <regex>
#include <string>
#include <tuple>
#include <vector>

namespace pigcd {
namespace webapi {

extern std::string static_files_directory;
extern std::string server_host;
extern std::string server_port;

struct request_data_t {
    std::string method;
    std::string url;  /// not decoded url (with %20 and other undecoded characters)
    std::vector<std::pair<std::string, std::string>> headers;
    std::vector<char> data;
    /**
     * @brief decode uriencoded string into regular string
     *
     * @param uriencoded Encoded string, like "/ind%20e%20%C5%82%C4%85%C4%87%C5%BCx.=html"
     * @return std::string decoded string like "/ind e łąćżx.=html"
     */
    static std::string decode(const std::string &uriencoded);
    /**
     * @brief Get the path object
     *
     * @return std::string return the path part of the url. It is before '?' character.
     */
    std::string get_path() const;
    /**
     * @brief Get the path array object that is constructed by splitting the path by the '/' character.
     *
     * @return std::vector<std::string> the list of url path elements. For paht /x/y it will be {"x","y"}
     */
    std::vector<std::string> get_path_array() const;
    /**
     * @brief Get the query object that contains the list of query attributes
     *
     * @return std::vector<std::pair<std::string, std::string>> the key,value pairs of query arguments
     */
    std::vector<std::pair<std::string, std::string>> get_query() const;
};

/**
 * @brief The datatype that represents response. It conatins minimal set of values. The body is provided as an istream so it is easy to read from file or even
 * standard input.
 */
struct response_data_t {
    std::shared_ptr<int> client_socket;  // this must be shared because if send data, this socket will be invalid
    std::string protocol;
    int status;
    std::string status_comment;
    std::vector<std::pair<std::string, std::string>> headers;

    int send(std::shared_ptr<std::istream> body);
    int send(std::string body);
};

/**
 * @brief the request handler type
 *
 */
using on_request_f = std::function<void(pigcd::webapi::request_data_t &request, pigcd::webapi::response_data_t &response, const std::vector<std::string> &params)>;

struct api_mapping_t {
    std::regex method;
    std::string method_string;
    std::regex match;
    std::string match_string;
    on_request_f handler;
    std::string name;
    std::string description;
};

class mini_http_c {
   protected:
    std::atomic<bool> running_server;
    std::vector<std::tuple<int, std::string, std::string>> listen_sockets;

   public:
    std::list<api_mapping_t> mappings;

    void on_request(std::string method, std::string match, on_request_f handler, std::string name = "", std::string desciption = "");
    mini_http_c(std::string addr, std::string port);

    // void server();
    void server(std::function<bool()> heart_beat);

    std::string generate_apidoc();
    virtual ~mini_http_c();
};

void handle_static(request_data_t &request, response_data_t &response_, const std::vector<std::string> &);
/**
 * @brief Splits the string into parts divided by the split_char
 *
 * @param input the string, for example "abc&xyz&bbb"
 * @param split_char the character, for example '&'
 * @return std::vector<std::string> array of substrings, for example {"abc","xyz","bbb"}.
 */
std::vector<std::string> split(const std::string input, char split_char);
}  // namespace webapi
}  // namespace pigcd
#endif
