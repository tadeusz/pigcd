/**
 * @file unit_conversion.cpp
 * @author Tadeusz Puźniakowski
 * @brief
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#include "unit_conversion.hpp"

#include <functional>

namespace pigcd {
namespace unit_conversion {

position_t steps_per_millimeter = {100, 100, -1600};  // todo

std::function<motors::machine_position_t(const position_t& distances_)> to_steps = cartesian_to_steps;
std::function<position_t(const motors::machine_position_t& steps_)> to_xyz = steps_to_cartesian;

motors::machine_position_t cartesian_to_steps_corexy(const position_t& distances_) {
    return {(int)((distances_[0] * scales_[0] + distances_[1] * scales_[1]) * steps_per_millimeter[0]),
            (int)((distances_[0] * scales_[0] - distances_[1] * scales_[1]) * steps_per_millimeter[1]),
            (int)(distances_[2] * steps_per_millimeter[2] * scales_[2])};
}

position_t steps_to_cartesian_corexy(const motors::machine_position_t& steps_) {
    position_t ret;
    ret[0] = 0.5 * (double)(steps_[0] / steps_per_millimeter[0] + steps_[1] / steps_per_millimeter[1]) / scales_[0];
    ret[1] = 0.5 * (double)(steps_[0] / steps_per_millimeter[0] - steps_[1] / steps_per_millimeter[1]) / scales_[1];
    ret[2] = steps_[2] / (steps_per_millimeter[2] * scales_[2]);
    return ret;
}

motors::machine_position_t cartesian_to_steps(const position_t& distances_) {
    motors::machine_position_t ret;
    for (std::size_t i = 0; i < distances_.size(); i++) ret[i] = distances_[i] * steps_per_millimeter[i] * scales_[i];
    return ret;
}

position_t steps_to_cartesian(const motors::machine_position_t& steps_) {
    position_t ret;
    for (std::size_t i = 0; i < steps_.size(); i++) ret[i] = steps_[i] / (steps_per_millimeter[i] * scales_[i]);
    return ret;
}

}  // namespace unit_conversion
}  // namespace pigcd