/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "webapi.hpp"

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/signal.h>
#include <unistd.h>

#include <any>
#include <chrono>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>

namespace pigcd {
namespace webapi {

std::string static_files_directory = "static";
std::string server_host = "0.0.0.0";
std::string server_port = "12112";

std::string ip_to_string(struct addrinfo *lst) {
    /* IPv4 */
    char ipv4[INET_ADDRSTRLEN];
    struct sockaddr_in *addr4;

    /* IPv6 */
    char ipv6[INET6_ADDRSTRLEN];
    struct sockaddr_in6 *addr6;

    if (lst != NULL) {
        if (lst->ai_addr->sa_family == AF_INET) {
            addr4 = (struct sockaddr_in *)lst->ai_addr;
            inet_ntop(AF_INET, &addr4->sin_addr, ipv4, INET_ADDRSTRLEN);
            return ipv4;
        } else if (lst->ai_addr->sa_family == AF_INET6) {
            addr6 = (struct sockaddr_in6 *)lst->ai_addr;
            inet_ntop(AF_INET6, &addr6->sin6_addr, ipv6, INET6_ADDRSTRLEN);
            return ipv6;
        }
    }
    throw std::invalid_argument("Could not translate ai_addr into string");
}

/**
 * @brief Start listening server on the host and port
 *
 * @param host host name to start server
 * @param port port number or service name
 * @return std::tuple<int,std::string, std::string> the values of
 * {listening_socket, actual_host, actual_port}
 */
std::tuple<int, std::string, std::string> do_listen(std::string host, std::string port) {
    int yes = 1;
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int s;
    int sdd;

    for (unsigned int i = 0; i < sizeof(struct addrinfo); i++) ((char *)&hints)[i] = 0;
    hints.ai_family = AF_UNSPEC;      // Allow IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM;  // Datagram socket
    hints.ai_flags = AI_PASSIVE;      // For wildcard IP address
    hints.ai_protocol = 0;            // Any protocol
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    s = getaddrinfo(host.c_str(), port.c_str(), &hints, &result);
    if (s != 0) {
        std::stringstream ss;
        ss << "ListeningSocket getaddrinfo:: " << gai_strerror(s) << "; port= " << port;
        throw std::invalid_argument(ss.str());
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sdd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sdd == -1) continue;

        if (setsockopt(sdd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
            throw std::invalid_argument(
                "error at: setsockopt( "
                "sfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof( int ) )");
        }
        auto flags = fcntl(sdd, F_GETFL, 0);
        fcntl(sdd, F_SETFL, flags | O_NONBLOCK);

        if (bind(sdd, rp->ai_addr, rp->ai_addrlen) == 0) {
            host = ip_to_string(rp);
            break;
        }
        ::close(sdd);
    }

    if (rp == NULL) {
        std::stringstream ss;
        ss << "ListeningSocket unable to bind address:: " << gai_strerror(s) << "; " << port;
        throw std::invalid_argument(ss.str());
    }
    freeaddrinfo(result);
    if (listen(sdd, 32) == -1) {
        throw std::invalid_argument("listen error");
    }

    return {sdd, host, port};
}

/**
 * @brief accepts connection on listening socket
 *
 * @param sfd listening socket file descriptor
 * @return std::tuple<int,std::string, std::string> connected socket, host_name,
 * port_name
 */
std::tuple<int, std::string, std::string> do_accept(int sfd) {
    struct sockaddr_storage peer_addr;
    socklen_t peer_addr_len = sizeof(struct sockaddr_storage);

    std::string host_s;
    std::string service_s;
    int csfd;

    if ((csfd = ::accept(sfd, (struct sockaddr *)&peer_addr, &peer_addr_len)) == -1) {
        if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
            return {-1, "0.0.0.0", "EAGAIN EWOULDBLOCK"};
        }
        throw std::invalid_argument("could not accept connection!");
    } else {
        char host[NI_MAXHOST], service[NI_MAXSERV];
        getnameinfo((struct sockaddr *)&peer_addr, peer_addr_len, host, NI_MAXHOST, service, NI_MAXSERV, NI_NUMERICSERV);
        host_s = host;
        service_s = service;
    }
    return {csfd, host_s, service_s};
}

/**
 * @brief read the request line. In case of some unexpected situation it will
 * report to standard output
 *
 * @param s
 * @return std::string
 */
std::string read_line(int s) {
    char c[2] = {0, 0};
    std::string ret = "";
    auto bytesRead = ::read(s, c, 1);
    while (bytesRead > 0) {
        ret = ret + c[0];
        if ((c[0] == '\n') && (c[1] == '\r')) {
            break;
        }
        c[1] = c[0];
        bytesRead = ::read(s, c, 1);
        if (bytesRead < 0) {
            throw std::invalid_argument("Error reading HTTP Request line: bytesRead:" + std::to_string(bytesRead) + "; ERRNO:" + hstrerror(errno));
        }
    }
    if (ret.length() >= 2) return ret.substr(0, ret.length() - 2);
    return ret;
}

std::tuple<std::string, std::string, std::string> method_line(std::string line) {
    auto delim = line.find(':');
    std::vector<std::string> parts;
    while ((delim = line.find(' ')) != std::string::npos) {
        parts.push_back(line.substr(0, delim));
        line = line.substr(delim + 1);
    }
    parts.push_back(line);
    if (parts.size() == 3) {
        return {parts[0], parts[1], parts[2]};
    } else
        return {"", "", ""};
}

std::pair<std::string, std::string> response_status_line(std::string line) {
    std::stringstream ss(line);
    std::string version, status_code;
    ss >> version >> status_code;
    return {version, status_code};
}

std::pair<std::string, std::string> header_line(std::string line) {
    auto delim = line.find(':');
    if (delim == std::string::npos) {
        return {"", ""};
    }
    std::transform(line.begin(), line.begin() + delim, line.begin(), [](unsigned char c) { return std::tolower(c); });
    return {line.substr(0, delim), line.substr(delim + 1)};
}

std::vector<std::string> get_header_values(const std::vector<std::pair<std::string, std::string>> &e, std::string key) {
    std::vector<std::string> ret;
    for (auto [k, v] : e)
        if (k == key) ret.push_back(v);
    return ret;
}

std::string generate_response_begin(int code = 200, std::string code_comment = "ok") {
    std::stringstream sent;
    sent << "HTTP/1.1 " << code << " - " << code_comment;
    sent << "\r\nServer: pigcd " << __DATE__;
    sent << "\r\nConnection: close";
    sent << "\r\n\r\n";

    return sent.str();
}

/**
 * @brief Get the header value object
 *
 * @tparam T target value type
 * @param e headers vector. Last header with the given key will be considered
 * @param key key to find. All lower case letters. For example "content-type"
 * @param default_value the default value. The type will be determined by this
 * value.
 * @return T value of the given header or default_value if none was found
 */
template <typename T>
T get_header_value(const std::vector<std::pair<std::string, std::string>> &e, std::string key, T default_value) {
    std::any ret = default_value;
    for (auto [k, v] : e) {
        if (k == key) {
            if (std::is_same_v<T, int>)
                ret = stoi(v);
            else if (std::is_same_v<T, double>)
                ret = stod(v);
            else if (std::is_same_v<T, unsigned>)
                ret = (unsigned)stoul(v);
            else if (std::is_same_v<T, unsigned long>)
                ret = stoul(v);
            else if (std::is_same_v<T, size_t>)
                ret = stoul(v);
            else if (std::is_same_v<T, char>)
                ret = v.size() ? v.at(0) : default_value;
            else
                ret = v;
        }
    }
    return std::any_cast<T>(ret);
}

std::pair<std::vector<std::pair<std::string, std::string>>, std::vector<char>> read_headers_and_body(int client_socket) {
    std::string line;
    std::vector<std::pair<std::string, std::string>> headers;
    while ((line = read_line(client_socket)).size() > 0) {
        headers.push_back(header_line(line));
        auto [key, value] = header_line(line);
    }
    size_t data_size = get_header_value(headers, "content-length", 0);
    std::vector<char> data(data_size);
    if (data_size > 0) {
        read(client_socket, data.data(), data_size);
    }
    shutdown(client_socket, 0);
    return {headers, data};
}

std::pair<response_data_t, std::string> send_request(std::string addr, std::string port, const request_data_t request) {
    auto [method, url, headers, body] = request;
    // std::vector<std::pair<std::string, std::string>> headers;
    headers.push_back({"host", addr + ":" + port});
    headers.push_back({"connection", "close"});

    struct addrinfo hints;
    // bzero((char *)&hints, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;      ///< IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM;  ///< stream socket

    struct addrinfo *addr_p;
    int err = getaddrinfo(addr.c_str(), port.c_str(), &hints, &addr_p);
    if (err) {
        throw std::runtime_error(gai_strerror(err));
    }
    struct addrinfo *rp;
    // find first working address that we can connect to
    for (rp = addr_p; rp != NULL; rp = rp->ai_next) {
        int connected_socket = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (connected_socket != -1) {
            if (connect(connected_socket, rp->ai_addr, rp->ai_addrlen) != -1) {
                std::stringstream request_stream;
                request_stream << method << " " << url << " "
                               << "HTTP/1.1"
                               << "\r\n";
                if (body.size())
                    request_stream << "content-length"
                                   << ": " << body.size() << "\r\n";
                for (auto [k, v] : headers) {
                    if (k != "content-length") request_stream << k << ": " << v << "\r\n";
                }
                request_stream << "\r\n";
                auto headers_and_requ = request_stream.str();
                write(connected_socket, headers_and_requ.data(), headers_and_requ.size());
                if (body.size()) write(connected_socket, body.data(), body.size());
                shutdown(connected_socket, 1);
                std::string line = read_line(connected_socket);

                auto [version, status_code] = response_status_line(line);
                auto [headers, data] = read_headers_and_body(connected_socket);

                close(connected_socket);

                freeaddrinfo(addr_p);  // remember - cleanup
                return {{std::make_shared<int>(-1), version, std::stoi(status_code), "---", headers}, std::string(data.begin(), data.end())};
            }
        }
    }
    freeaddrinfo(addr_p);
    throw std::invalid_argument(std::string("could not open connection to ") + addr + ":" + port);
}

std::string request_data_t::decode(const std::string &uriencoded) {
    using namespace std;  // inspired by https://www.programcreek.com/cpp/?CodeExample=url+decode
    string url_ret = uriencoded;
    string url;
    string::iterator cursor = url_ret.begin();
    while (cursor != url_ret.end()) {
        if (cursor[0] == '+') {
            url += ' ';
            cursor++;
        } else if ((cursor[0] == '%') && (distance(cursor, url_ret.end()) > 2) && isxdigit(cursor[1]) && isxdigit(cursor[2])) {
            url += (char)std::stoi(string{cursor[1], cursor[2]}, 0, 16);
            cursor += 3;
        } else {
            url += *cursor++;
        }
    }
    return url;
}
/**
 * @brief Splits the string into parts divided by the split_char
 *
 * @param input the string, for example "abc&xyz&bbb"
 * @param split_char the character, for example '&'
 * @return std::vector<std::string> array of substrings, for example {"abc","xyz","bbb"}.
 */
std::vector<std::string> split(const std::string input, char split_char) {
    std::vector<std::string> ret = {""};
    for (auto cursor : input)
        if (cursor == split_char) {
            ret.push_back("");
        } else {
            ret.back() += cursor;
        }
    return ret;
}

std::vector<std::string> get_params(std::string &url, std::regex &url_match) {
    std::smatch base_match;
    if (std::regex_match(url, base_match, url_match) && std::regex_match(url, base_match, url_match)) {
        std::vector<std::string> ret(base_match.size());

        for (size_t i = 0; i < base_match.size(); ++i) {
            ret[i] = base_match[i].str();
        }
        return ret;
    }
    return {};
}

std::string request_data_t::get_path() const {
    auto query_div = url.find_first_of('?');
    if (query_div != std::string::npos) {
        return decode(url.substr(0, query_div));
    } else
        return decode(url);
}
std::vector<std::string> request_data_t::get_path_array() const {
    auto path = get_path();
    if (path.size() > 0) path = path.substr(1);  // remove "/" from path
    return split(path, '/');
}
std::vector<std::pair<std::string, std::string>> request_data_t::get_query() const {
    auto query_div = url.find_first_of('?');
    auto arguments = (query_div != std::string::npos) ? (url.substr(query_div + 1)) : "";
    std::vector<std::pair<std::string, std::string>> ret;
    for (auto kv : split(arguments, '&')) {
        auto kva = split(kv, '=');
        if (kva.size() == 2) ret.push_back({kva.front(), kva.back()});
    }
    return ret;
}

/////////////

mini_http_c::mini_http_c(std::string host, std::string port) {
    // listen_sockets
    int yes = 1;
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int s;
    int sdd;

    for (unsigned int i = 0; i < sizeof(struct addrinfo); i++) ((char *)&hints)[i] = 0;
    hints.ai_family = AF_UNSPEC;      // Allow IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM;  // Datagram socket
    hints.ai_flags = AI_PASSIVE;      // For wildcard IP address
    hints.ai_protocol = 0;            // Any protocol
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    s = getaddrinfo(host.c_str(), port.c_str(), &hints, &result);
    if (s != 0) {
        std::stringstream ss;
        ss << "ListeningSocket getaddrinfo:: " << gai_strerror(s) << "; port= " << port;
        throw std::invalid_argument(ss.str());
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sdd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sdd == -1) continue;

        if (setsockopt(sdd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
            throw std::invalid_argument(
                "error at: setsockopt( "
                "sfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof( int ) )");
        }
        auto flags = fcntl(sdd, F_GETFL, 0);
        fcntl(sdd, F_SETFL, flags | O_NONBLOCK);

        if (bind(sdd, rp->ai_addr, rp->ai_addrlen) == 0) {
            host = ip_to_string(rp);
            if (listen(sdd, 32) == -1) {
                throw std::invalid_argument("listen error");
            }
            listen_sockets.push_back({sdd, host, port});
            continue;
        }
        ::close(sdd);
    }

    if (listen_sockets.size() == 0) {
        std::stringstream ss;
        ss << "Unable to bind address:: " << gai_strerror(s) << "; " << port;
        throw std::invalid_argument(ss.str());
    }
    freeaddrinfo(result);
}

int send_headers(response_data_t &response) {
    std::stringstream sent;
    auto &[client_socket_p, protocol, status, status_comment, response_headers] = response;
    auto &client_socket = *client_socket_p;
    sent << protocol << " " << status << " - " << status_comment;
    for (auto [k, v] : response_headers) {
        sent << "\r\n" << k << ": " << v;
    }
    sent << "\r\n\r\n";
    auto sent_str = sent.str();
    int sent_bytes = 0;
    sent_bytes = write(client_socket, sent_str.data(), sent_str.size());
    if (sent_bytes < 0) throw std::domain_error("Could not send response headers to client");
    return sent_bytes;
}

int response_data_t::send(std::shared_ptr<std::istream> body) {
    int written_bytes = 0;
    int &s = *client_socket;
    int sent_bytes = 0;
    send_headers(*this);
    std::vector<char> buffer;
    int rbyte = body->get();
    while (rbyte != EOF) {
        buffer.push_back((char)rbyte);
        rbyte = body->get();
        if ((buffer.size() > 65530) || (rbyte == EOF)) {
            written_bytes += (sent_bytes = write(s, buffer.data(), buffer.size()));
            if (sent_bytes < 0) {
                written_bytes = -1;  // error;
                break;
            }
            buffer.clear();
        }
    }
    close(s);
    *client_socket = -1;
    return written_bytes;
}

int response_data_t::send(std::string body) {
    int sent_bytes = 0;
    send_headers(*this);
    sent_bytes = write(*client_socket, body.data(), body.size());
    close(*client_socket);
    *client_socket = -1;
    return sent_bytes;
}

void mini_http_c::on_request(std::string method, std::string match, on_request_f handler, std::string name, std::string description) {
    api_mapping_t mapping;
    mapping.method = std::regex(method);
    mapping.match = std::regex(match);
    mapping.method_string = method;
    mapping.match_string = match;
    mapping.handler = handler;
    mapping.name = (name == "") ? (method + " " + match) : name;
    mapping.description = description;
    mappings.push_back(mapping);
}

request_data_t recv_request(int client_socket) {
    std::string line = read_line(client_socket);
    auto [method, url, version] = method_line(line);
    auto [headers, data] = read_headers_and_body(client_socket);
    return {method, url, headers, data};
}

void mini_http_c::server(std::function<bool()> heart_beat) {
    running_server = true;
    using namespace std::chrono_literals;

    for (auto [srv_socket, srv_addr, srv_port] : listen_sockets) {
        std::cout << "Listening on: " << srv_addr << " : " << srv_port << std::endl;
    }

    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGPIPE);
    if (sigprocmask(SIG_BLOCK, &set, NULL) != 0) {
        throw std::invalid_argument("cannot set pthread_sigmask");
    }

    do {
        bool should_sleep = false;
        for (auto [srv_socket, srv_addr, srv_port] : listen_sockets) {
            auto [client_socket, client_addr, client_port] = do_accept(srv_socket);
            if (client_socket == -1) {
                should_sleep = true;
            } else
                try {
                    request_data_t request = recv_request(client_socket);
                    pigcd::webapi::response_data_t response = {std::make_shared<int>(client_socket),
                                                               "HTTP/1.1",
                                                               200,  // status code
                                                               "ok",
                                                               {{"connection", "close"}, {"Server", std::string("pigcd ") + __DATE__}}};

                    try {
                        for (auto i = mappings.begin(); (i != mappings.end()) && (*response.client_socket != -1); i++) {
                            std::smatch base_match;
                            if (std::regex_match(request.method, base_match, i->method)) {
                                auto params = get_params(request.url, i->match);
                                if (params.size() > 0) {
                                    i->handler(request, response, params);
                                }
                            }
                        }
                        if (*response.client_socket != -1) response.send("The request was not handled properly. Method response.send(...) was never run.");
                    } catch (const std::exception &e) {
                        response.status = 500;
                        response.status_comment = "internal server error";
                        response.send(std::string("something bad happened: ") + e.what());
                    }
                } catch (const std::invalid_argument &e) {
                    std::cerr << "Request malformed: " << e.what() << std::endl;
                }
        }
        if (should_sleep) std::this_thread::sleep_for(250ms);
    } while (running_server && heart_beat());
    running_server = false;
}

mini_http_c::~mini_http_c() {
    using namespace std::chrono_literals;
    running_server = false;
    std::this_thread::sleep_for(1s);
    for (auto [srv_socket, srv_addr, srv_port] : listen_sockets) {
        close(srv_socket);
    }
}

std::string mini_http_c::generate_apidoc() {
    using namespace pigcd;

    std::string ret;
    ret += R"--(<!DOCTYPE html>
<html style="font-family: Segoe UI;">
<head>
    <title>pigcd - WebAPI documentation</title>
</head>
<body>)--";
    ret += "<ul>";
    for (auto &mapping : this->mappings) {
        std::string name = mapping.name;
        if (mapping.name == "") name = mapping.method_string + " " + mapping.match_string;
        ret = ret + "<div><h3>" + name + "</h3><code>" + mapping.method_string + "</code> <code>" + mapping.match_string + "</code> <div>" +
              mapping.description + "</div></div>\n";
    }
    ret = ret + "</ul>";
    ret += "</body>";
    return ret;
}

std::map<std::string, std::string> load_system_mime_types() {
    std::ifstream f("/etc/mime.types");
    std::map<std::string, std::string> ret;
    if (f.is_open()) {
        for (std::string line = ""; std::getline(f, line);) {
            if (line.size() == 0) continue;
            if (line.find('#') != std::string::npos) continue;
            std::stringstream ss(line);
            std::string mime_type;
            std::string ext;
            ss >> mime_type;
            while (ss >> ext) ret[ext] = mime_type;
        }
        return ret;
    } else {
        ret["html"] = "text/html";
        ret["png"] = "image/png";
        ret["css"] = "text/css";
        ret["js"] = "application/json";
        ret["ico"] = "image/ico";
        ret["svg"] = "image/svg+xml";
        return ret;
    }
}

std::string content_type_from_fname(const std::string &path) {
    static auto mime = load_system_mime_types();
    auto suffix = path.substr(path.find_last_of('.') + 1);
    std::transform(suffix.begin(), suffix.end(), suffix.begin(), [](auto c) { return std::tolower(c); });
    return (mime.count(suffix)) ? (mime[suffix]) : "text/plain";
}

void serve_file(std::string path, response_data_t &response) {
    using namespace std;

    auto &response_headers = response.headers;
    if (path.size() > 4) {
        if (ifstream(path).good()) {
            response_headers.push_back({"content-type", content_type_from_fname(path)});
            response.send(std::make_shared<std::ifstream>(path));
        } else {
            response.status = 404;
            response.status_comment = "not found";
            response_headers.push_back({"content-type", "text/html"});
            response.send("File not found.");
        }
    }
}

void handle_static(request_data_t &request, response_data_t &response_, const std::vector<std::string> &) {
    using namespace std;
    response_data_t response = response_;
    auto [method, url, headers, body] = request;

    auto body_string = string(body.begin(), body.end());
    vector<string> url_array = request.get_path_array();
    if (url_array.size() > 0) {
        if (url_array[0] == "") url_array[0] = "index.html";
        std::string path = static_files_directory;
        for (auto pelem : url_array) {
            if (pelem != "..") path += "/" + pelem;
        }

        serve_file(path, response);
    } else {
        response.send(
            "<!DOCTYPE html><html lang='en'><head><meta "
            "charset='utf-8'><title>File not found</title></head><body><h1>File not found</h1></body></html>");
    }
}

}  // namespace webapi
}  // namespace pigcd