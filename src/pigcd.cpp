/**
 * @file main.cpp
 * @author Tadeusz Puźniakowski
 * @brief Allows for execution of simple gcode directly on raspberry pi
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <signal.h>

#include <chrono>
#include <distance_t.hpp>
#include <fstream>
#include <iostream>
#include <json.hpp>
#include <list>
#include <map>
#include <random>
#include <regex>
#include <sstream>
#include <thread>
#include <vector>

#include "buttons.hpp"
#include "configuration.hpp"
#include "gcd.hpp"
#include "gpio.hpp"
#include "motors.hpp"
#include "s_gcode_executor.hpp"
#include "unit_conversion.hpp"
#include "vec3di.hpp"
#include "webapi.hpp"

using namespace tp::discrete;

namespace pigcd {

namespace gpio {
#ifdef ARCH_x86_64
#warning "Using fake driver on x86 architecture. If you want it to work, compile it on the Raspberry Pi 2 or 3"
extern std::list<std::vector<unsigned int>> reported_fake_states;
void print_reported_fake_states();
#endif
}  // namespace gpio
}  // namespace pigcd

pigcd::service::gcode_executor_t gcode_executor_server_obj;

void request_handler_GET_api_gcode_status(pigcd::webapi::request_data_t &, pigcd::webapi::response_data_t &response,
                                          const std::vector<std::string> &params) {
    try {
        response.send(gcode_executor_server_obj.get_status(std::stoi(params.at(1))).dump());
    } catch (std::exception &e) {
        std::string error;
        error = "error parsing params: ";
        for (auto p : params) {
            error = error + "["+ p +"] ";
        }
        std::cerr << error << std::endl;
        throw std::invalid_argument(error);
    }
    
}
void request_handler_GET_api_gcode_status_all(pigcd::webapi::request_data_t &, pigcd::webapi::response_data_t &response,
                                              const std::vector<std::string> &) {
    response.send(gcode_executor_server_obj.get_status(0).dump());
}

void request_handler_POST_exec(pigcd::webapi::request_data_t &request, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
    using namespace pigcd;

    auto [method, url, headers, body] = request;

    auto body_string = std::string(body.begin(), body.end());

    std::stringstream input_lines(body_string);
    std::list<std::string> lines;
    std::string l;
    while (std::getline(input_lines, l)) lines.push_back(l);
    response.send(gcode_executor_server_obj.exec(lines).dump());
}

void on_ctrl_c_signal(int) {
    pigcd::motors::is_thread_working = 0;  // terminate motors thread
    gcode_executor_server_obj.ignore_next_gcode = true;
}

void on_usr1_signal(int) { gcode_executor_server_obj.ignore_next_gcode = true; }

int main(int argc, char **argv) {
    using namespace pigcd;
    using namespace std::chrono_literals;

    if (config::from_args(argc, argv) < 0) return 0;
    ;
    std::ifstream cfg(config::config_file_name);
    if (cfg.is_open()) {
        config::load(cfg);
    }
    config::from_args(argc, argv);  ///< make sure that the arguments from CLI are more important than the ones from file
    config::save(std::cout);
    motors::init();
    gpio::init_in(buttons::io_buttons, std::vector<int>(buttons::io_buttons.size(), 1));

    auto steps_thread = motors::run_thread();
    std::string line;
    signal(SIGINT, on_ctrl_c_signal);
    signal(SIGUSR1, on_usr1_signal);

    webapi::mini_http_c srv(webapi::server_host, webapi::server_port);

    // pigcd::webapi::add_api_doc_handler();
    srv.on_request("GET", "/api/gcode_status", request_handler_GET_api_gcode_status_all, "Execution status", "Returns the status for the g-code execution.");
    srv.on_request("GET", "/api/gcode_status/([[:digit:]]+)", request_handler_GET_api_gcode_status, "Remaining execution status",
                   "Returns the execution status for every line starting with ([[:digit:]]+).");
    srv.on_request(
        "GET", "/apidoc",
        [&srv](pigcd::webapi::request_data_t &, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
            response.send(srv.generate_apidoc());
        },
        "WebAPI Documentatino", "Generates the documentation page.");
    srv.on_request("POST", "/api/exec", request_handler_POST_exec, "Start gcode",
                   "Executes the gcode program. There can be only one executing, so if machine is running some gcode program, it will return error.");
    srv.on_request("GET", ".*", pigcd::webapi::handle_static, "Static contents", "Returns the static demo for the API");

    // everything is ready. Start server
    srv.server([]() { return !(pigcd::motors::is_thread_working == 0); });

    pigcd::motors::is_thread_working = false;
    int ret = steps_thread.get();

    motors::disable_spindle(0, unit_conversion::seconds_to_ticks(1));
    return ret;
}
