/**
 * @file motors.cpp
 * @author Tadeusz Puźniakowski
 * @brief
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#include "motors.hpp"

#include <time.h>

#include <array>
#include <cerrno>
#include <chrono>
#include <cstring>
#include <iostream>
#include <mutex>
#include <queue>
#include <stdexcept>
#include <thread>
#include <vector>

#include "buttons.hpp"
#include "configuration.hpp"
#include "gpio.hpp"

namespace pigcd {

auto chrono_time_start = std::chrono::high_resolution_clock::now();

uint64_t get_precision_time() { return (std::chrono::high_resolution_clock::now() - chrono_time_start).count(); }

inline void set_thread_realtime() {
    sched_param sch_params;
    sch_params.sched_priority = sched_get_priority_max(SCHED_RR);
    if (pthread_setschedparam(pthread_self(), SCHED_RR, &sch_params)) {
        static int already_warned = 0;
        if (already_warned == 0) {
            std::cerr << "Warning: Failed to set Thread scheduling : " << std::strerror(errno) << std::endl;
            already_warned++;
        }
    }
}

namespace motors {

uint64_t step_duration = 150000;  // how long should we wait till next step

std::vector<int> io_steppers_step = {22, 17, 11};
std::vector<int> io_steppers_dir = {27, 4, 9};
std::vector<int> io_steppers_en = {10, 10, 10};
std::vector<int> io_spindles = {18};

void init() {
    static int already_started = 0;
    if (already_started) throw std::invalid_argument("you cannot run init twice!");
    if (motors::io_steppers_step.size() != 3) throw std::invalid_argument("size of motors::io_steppers_step must be 3");
    if (motors::io_steppers_dir.size() != 3) throw std::invalid_argument("size of motors::io_steppers_dir must be 3");
    if (motors::io_steppers_en.size() != 3) throw std::invalid_argument("size of motors::io_steppers_en must be 3");
    gpio::init_out(motors::io_steppers_step);
    gpio::init_out(motors::io_steppers_dir);
    gpio::init_out(motors::io_steppers_en);
    gpio::init_out(motors::io_spindles);
    already_started = 1;
}

using steps_increasing_buffer_t = std::pair<machine_position_t, int>;
std::array<steps_increasing_buffer_t, 1024> steps_increasing_buffer;
unsigned steps_increasing_buffer_read = 0;
unsigned steps_increasing_buffer_write = 0;
std::mutex steps_increasing_buffer_mutex;

/**
 * @brief pushes steps count to perform on each axis
 *
 * @param steps_ {dx,dy,dz}
 * @return unsigned size of buffer. If 0 then the buffer is full and you should
 * retry push command
 */
unsigned steps_increasing_buffer_push(steps_increasing_buffer_t steps_) {
    std::lock_guard<std::mutex> lock(steps_increasing_buffer_mutex);
    if (((steps_increasing_buffer_write + 1) % steps_increasing_buffer.size()) == steps_increasing_buffer_read)
        return (steps_increasing_buffer.size() + steps_increasing_buffer_write - steps_increasing_buffer_read) % steps_increasing_buffer.size();
    steps_increasing_buffer[steps_increasing_buffer_write] = steps_;
    steps_increasing_buffer_write = (steps_increasing_buffer_write + 1) % steps_increasing_buffer.size();
    return 0;
}

steps_increasing_buffer_t steps_increasing_buffer_pop() {
    std::lock_guard<std::mutex> lock(steps_increasing_buffer_mutex);
    if (steps_increasing_buffer_write == steps_increasing_buffer_read) return {{0, 0, 0}, 0};
    auto steps_ = steps_increasing_buffer[steps_increasing_buffer_read];
    steps_increasing_buffer_read = (steps_increasing_buffer_read + 1) % steps_increasing_buffer.size();
    return steps_;
}

inline int steps_increasing_buffer_size__no_threadsafe_() {
    return (steps_increasing_buffer.size() + steps_increasing_buffer_write - steps_increasing_buffer_read) % steps_increasing_buffer.size();
}

int steps_increasing_buffer_size() {
    std::lock_guard<std::mutex> lock(steps_increasing_buffer_mutex);
    return (steps_increasing_buffer.size() + steps_increasing_buffer_write - steps_increasing_buffer_read) % steps_increasing_buffer.size();
}

void steps_increasing_buffer_clear() {
    std::lock_guard<std::mutex> lock(steps_increasing_buffer_mutex);
    steps_increasing_buffer_write = 0;
    steps_increasing_buffer_read = 0;
}

volatile std::atomic<int> is_thread_working = 0;

void enable_motors() {
    using namespace std::chrono_literals;
    // enable steppers
    gpio::set_states(motors::io_steppers_en, std::vector<char>(motors::io_steppers_en.size(), 0));
    std::this_thread::sleep_for(100000000ns);
}

void disable_motors() {
    // disable steppers
    gpio::set_states(motors::io_steppers_en, std::vector<char>(motors::io_steppers_en.size(), 1));
}

void enable_spindle(int i, uint64_t delay) {
    using namespace std::chrono_literals;
    while (steps_increasing_buffer_size() > 0) std::this_thread::sleep_for(5ms);
    // enable spindle
    gpio::set_states({motors::io_spindles.at(i)}, {1});
    std::this_thread::sleep_for(std::chrono::nanoseconds(delay));
}

void disable_spindle(int i, uint64_t delay) {
    using namespace std::chrono_literals;
    while (steps_increasing_buffer_size() > 0) std::this_thread::sleep_for(5ms);
    // disable spindle

    std::this_thread::sleep_for(std::chrono::nanoseconds(delay));
    gpio::set_states({motors::io_spindles.at(i)}, {0});
}

std::pair<machine_position_t, int> probe_status__ = {{0, 0, 0}, 0};
std::pair<machine_position_t, int> get_probe_status() {
    std::lock_guard lock(steps_increasing_buffer_mutex);
    using namespace std::chrono_literals;
    return probe_status__;
}
void set_probe_status(const std::pair<machine_position_t, int> &status) {
    std::lock_guard lock(steps_increasing_buffer_mutex);
    using namespace std::chrono_literals;
    probe_status__ = status;
}

void reset_probe_status() {
    using namespace std::chrono_literals;
    std::lock_guard lock(steps_increasing_buffer_mutex);
    if (steps_increasing_buffer_size__no_threadsafe_() > 0)
        throw std::invalid_argument(
            "the steps increasing buffer must be empty. Please run "
            "reset_probe_status after it is cleared");
    probe_status__ = {{0, 0, 0}, 0};
    //  while (steps_increasing_buffer_size() > 0)
    //  std::this_thread::sleep_for(5ms);
}

/**
 * @brief Helper function that is meant to be inlined. It checks if there is no collision
 * with end stop sensors. If there is collision with endstop
 * and the move should be performed it just cancels move.
 *
 * @param current_flags
 * @param move_it
 * @param now
 * @param probe_status
 * @return true - skip next steps because we cannot move forward
 * @return false - do the next steps in the runner thread
 */
inline auto run_thread_helper_check_state(int &current_flags, int &move_it, std::chrono::_V2::system_clock::time_point &now,
                                          pigcd::motors::steps_increasing_buffer_t &probe_status, pigcd::motors::machine_position_t &dp) -> bool {
    using namespace tp::discrete;
    if ((current_flags & PROBE) && (move_it)) {
        int state = ((current_flags & PROBE_X) ? gpio::get_state(buttons::io_buttons[0]) : 0) |
                    ((current_flags & PROBE_Y) ? (gpio::get_state(buttons::io_buttons[1]) << 1) : 0) |
                    ((current_flags & PROBE_Z) ? (gpio::get_state(buttons::io_buttons[2]) << 2) : 0);
        if (state) {
            probe_status.second = state;
            now = std::chrono::high_resolution_clock::now();
            move_it = 0;
            dp = {0, 0, 0};

        } else {
            probe_status.first = probe_status.first + dp;
        }
        set_probe_status(probe_status);
    }
    return (move_it == 0);  // you can go if there is no move to do
};

/**
 * @brief Returns the future object for the new running task executing the hardware execution thread. The async task is the main execution part
 * for moveing the machine - interacts with the physical gpio (via gpio.hpp)
 *
 * @return std::future<int> future that will be resolved when the machine is turned off (thread is stopped and no moves will be possible).
 */
std::future<int> run_thread() {
    is_thread_working = 1;
    return std::async(std::launch::async, []() -> int {
        using namespace std::chrono_literals;
        using namespace tp::discrete;
        disable_motors();
        set_thread_realtime();

        int motor_timeout = 0;
        auto now = std::chrono::high_resolution_clock::now();  // get_precision_time();
        machine_position_t dp = {0, 0, 0};

        std::vector<int> pins(dp.size());
        std::vector<char> states(dp.size());
        std::vector<int> pins_hi(dp.size());
        std::vector<char> states_hi(dp.size());
        std::vector<int> pins_lo(dp.size());
        std::vector<char> states_lo(dp.size());

        int current_flags = 0;
        while (true) {
            auto probe_status = get_probe_status();
            if (probe_status.second == 0) {  // when there is an error we need
                                             // to stop and wait for reset.
                if ((dp[0] == 0) && (dp[1] == 0) && (dp[2] == 0)) {
                    auto [add_dp, flags] = steps_increasing_buffer_pop();
                    dp = dp + add_dp;
                    current_flags = flags;
                } else {
                    int move_it = 0;  // should we move?
                    // dir
                    {
                        for (unsigned i = 0; i < dp.size(); i++) {
                            if (dp[i] > 0) {
                                pins[i] = io_steppers_dir[i];
                                states[i] = 1;
                                move_it = 1;
                            } else if (dp[i] < 0) {
                                pins[i] = io_steppers_dir[i];
                                states[i] = 0;
                                move_it = 1;
                            } else {
                                pins[i] = io_steppers_dir[i];
                                states[i] = 2;  // ignore
                            }
                        }
                        gpio::set_states(pins, states);
                        std::this_thread::sleep_for(std::chrono::nanoseconds(1000));
                    }
                    if (run_thread_helper_check_state(current_flags, move_it, now, probe_status, dp)) continue;
                    if (move_it == 1) {
                        // enable motors if we just started moving
                        if (motor_timeout == 0) {
                            enable_motors();
                            std::this_thread::sleep_for(50ms);
                            now = std::chrono::high_resolution_clock::now();
                        }
                        motor_timeout = 1000;
                        for (unsigned i = 0; i < dp.size(); i++) {
                            if (dp[i] != 0) {
                                pins_hi[i] = io_steppers_step[i];
                                states_hi[i] = 1;
                                pins_lo[i] = io_steppers_step[i];
                                states_lo[i] = 0;
                                int reducto = ((dp[i] > 0) ? -1 : 1);
                                dp[i] = dp[i] + reducto;
                            } else {
                                pins_hi[i] = io_steppers_step[i];
                                states_hi[i] = 2;  // ignore
                                pins_lo[i] = io_steppers_step[i];
                                states_lo[i] = 2;  // ignore
                            }
                        }
                        gpio::set_states(pins_hi, states_hi);
                        std::this_thread::sleep_for(6000ns);
                        gpio::set_states(pins_lo, states_lo);
                        std::this_thread::sleep_for(10000ns);
                    }
                }
            } else {
                steps_increasing_buffer_clear();
            }
            if (is_thread_working == 0) {
                if (steps_increasing_buffer_size() == 0) break;
            }
            if (motor_timeout > 0) {
                motor_timeout = motor_timeout - 1;
                if (motor_timeout == 0) disable_motors();
            }
            now = now + std::chrono::nanoseconds(step_duration);
            std::this_thread::sleep_until(now);
        }
        disable_motors();
        return 0;  // ok
    });
};

machine_position_t global_machine_steps = {0, 0, 0};

std::mutex global_machine_steps_mutex;

machine_position_t get_global_machine_steps() {
    std::lock_guard lock(global_machine_steps_mutex);
    return global_machine_steps;
}
void set_global_machine_steps(const machine_position_t &steps) {
    std::lock_guard lock(global_machine_steps_mutex);
    global_machine_steps = steps;
}

int raw_exec_steps(const machine_position_t steps_, raw_exec_steps_flag_e flags) {
    using namespace std::chrono_literals;
    std::lock_guard lock(global_machine_steps_mutex);

    int timeout = steps_increasing_buffer.size();  // flags = PROBE_XYZ;
    auto probe_status = get_probe_status();
    if (probe_status.second != 0) return probe_status.second;

    if (pigcd::motors::is_thread_working == 0) throw std::invalid_argument("executing thread is terminated");
    while ((steps_increasing_buffer_push({steps_, flags}) != 0) && (timeout > 0)) {
        std::this_thread::sleep_for(std::chrono::nanoseconds(step_duration * 5 / 2));
        timeout--;
    }
    std::this_thread::sleep_for(std::chrono::nanoseconds(step_duration / 10));
    if (timeout == 0) throw std::invalid_argument("Timeout in raw_exec_steps - this should not happen");
    for (unsigned i = 0; i < global_machine_steps.size(); i++) global_machine_steps[i] += steps_[i];
    return probe_status.second;
}

}  // namespace motors

}  // namespace pigcd
